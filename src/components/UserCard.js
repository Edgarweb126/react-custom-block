import officePic from '../public/mainImage.png'
import { __ } from "@wordpress/i18n";
import { useState, Fragment } from "@wordpress/element";
import { PanelBody, SelectControl} from "@wordpress/components";
import { InspectorControls } from "@wordpress/block-editor";



function UserCard() {

    const [ color, setColor ] = useState( '#f4f5f5' );
 

  return (
      
    <Fragment>
        <InspectorControls>  
            < PanelBody title={ __('') }>  
        
            <SelectControl
            label="Bg Colors"
            value={ color }
            options={ [
                { label: 'Dark', value: '#23373c' },
                { label: 'Primary', value: '#c1002a' },
                { label: 'Light', value: '#f4f5f5' },
            ] }
            onChange={ ( newColor ) => setColor( newColor) }
        />
      
            </PanelBody>   
        </InspectorControls>  
       
      
    
    
    <div className='userCard'>
        <div className="cardImage">
            <img src={officePic} alt="" />
        </div>
        <div className='cardText' style={{ backgroundColor: color}}>
            <h5>Demo Box</h5>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
            <a href="#"><span className='hoverLink'>mehr erfahren</span>  &#8594;</a>
        </div>
    </div>
      
    </Fragment>
  );
}

export default UserCard;
