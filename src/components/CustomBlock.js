import UserCard from './UserCard';
import UserNoPic from './UserNoPic';
import React from 'react';
import '/src/style.css';
import { __ } from "@wordpress/i18n";
import { Fragment, useState } from "@wordpress/element";
import { PanelBody } from "@wordpress/components";
import { InspectorControls } from "@wordpress/block-editor";
import { Button } from '@wordpress/components';

// *** Internal dependencies ***


function CustomBlock() {
    const [component, setComp] = useState(<UserCard />);
  

    
    return (
        <Fragment>
      <div className='App'>       
        <div> {component}</div>


        <InspectorControls>
        <PanelBody title={ __('') }>
        <Button onClick={()=> setComp (<UserNoPic />)}>Custom block without Picture</Button>
        <Button onClick={()=> setComp (<UserCard />)}>Custom block with Picture</Button>
        </PanelBody>
        </InspectorControls>
       


      </div>
      </Fragment>
     
      
      
    );
}

export default CustomBlock;
