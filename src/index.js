import { registerBlockType } from '@wordpress/blocks';
import CustomBlock from './components/CustomBlock';
import './style.css'

registerBlockType( 'myguten/test-block', {
	title: 'Edgar',
	icon: 'hammer',
	category: 'design',
	attributes: {
		
	},
	edit: (props) => (<CustomBlock />),
	save: () =>{
		return null;
	}
} );